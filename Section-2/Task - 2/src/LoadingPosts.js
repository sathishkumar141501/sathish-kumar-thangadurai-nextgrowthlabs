import { Audio } from "react-loader-spinner";
import "./LoadingPosts.css";
export const LoadingPosts = () => {
  return (
    <div className="spinner">
      <Audio
        height="80"
        width="80"
        radius="9"
        color="gray"
        ariaLabel="loading"
        wrapperStyle
        wrapperClass
      />
      <p>Hang on, Loading content</p>
    </div>
  );
};
