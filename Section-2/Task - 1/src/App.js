import React, { useState } from "react";
import "./App.css";
function App() {
  const [treeData, setTreeData] = useState([
    {
      name: "Documents",
      type: "folder",
      children: [
        {
          name: "Document1.jpg",
          type: "file",
        },
        {
          name: "Document2.jpg",
          type: "file",
        },
        {
          name: "Document3.jpg ",
          type: "file",
        },
      ],
    },
    {
      name: "Desktop",
      type: "folder",
      children: [
        {
          name: "Screenshot1.jpg",
          type: "file",
        },
        {
          name: "Videopal.mp4",
          type: "file",
        },
      ],
    },
    {
      name: "Downloads",
      type: "folder",
      children: [
        {
          name: "Drivers",
          type: "folder",
          children: [
            { name: "Printerdriver.dmg", type: "file" },
            { name: "Cameradriver.dmg", type: "file" },
          ],
        },
      ],
    },
    {
      name: "Applications",
      type: "folder",
      children: [
        {
          name: "Webstrom.dmg",
          type: "file",
        },
        {
          name: "Pycharm.dmg",
          type: "file",
        },
        {
          name: "Filezila.dmg",
          type: "file",
        },
        {
          name: "Mattermost.dmg",
          type: "file",
        },
      ],
    },
    {
      name: "Chromedriver.dmg",
      type: "file",
    },
  ]);

  function handleToggleFolder(folder) {
    folder.open = !folder.open;
    setTreeData([...treeData]);
  }

  function handleCreateFolder(folderName, parentFolder) {
    // create new folder and update state variable
    const newFolder = {
      name: folderName,
      type: "folder",
      children: [],
    };
    if (parentFolder) {
      parentFolder.children.push(newFolder);
      setTreeData([...treeData]);
    } else {
      setTreeData([...treeData, newFolder]);
    }
  }
  function handleCreateFile(fileName, parentFolder) {
    // create new file and update state variable
    const newFile = {
      name: fileName,
      type: "file",
    };
    if (parentFolder) {
      parentFolder.children.push(newFile);
      setTreeData([...treeData]);
    } else {
      setTreeData([...treeData, newFile]);
    }
  }

  function handleDeleteItem(item, parentFolder) {
    // delete item and update state variable
    if (parentFolder) {
      parentFolder.children = parentFolder.children.filter(
        (child) => child !== item
      );
      setTreeData([...treeData]);
    } else {
      setTreeData(treeData.filter((child) => child !== item));
    }
  }

  function handleRenameItem(item, newName) {
    // rename item and update state variable
    item.name = newName;
    setTreeData([...treeData]);
  }

  function renderTreeData(data, depth = 0) {
    return (
      <ul>
        {data.map((item) => {
          if (item.type === "file") {
            return (
              <li key={item.name}>
                <span className="file">{item.name}</span>
                <span className="actions">
                  <button
                    className="delete-item"
                    onClick={() => handleDeleteItem(item)}
                  >
                    Delete
                  </button>
                  <button
                    className="rename-item"
                    onClick={() =>
                      handleRenameItem(
                        item,
                        prompt("Enter new file name:", item.name)
                      )
                    }
                  >
                    Rename
                  </button>
                </span>
              </li>
            );
          } else {
            return (
              <li key={item.name}>
                <span
                  className="folder"
                  onClick={() => handleToggleFolder(item)}
                >
                  {item.open ? "-" : "+"} {item.name}
                </span>
                <span className="actions">
                  <button
                    className="new-folder"
                    onClick={() =>
                      handleCreateFolder(prompt("Enter folder name:"), item)
                    }
                  >
                    New Folder
                  </button>
                  <button
                    className="new-file"
                    onClick={() =>
                      handleCreateFile(prompt("Enter file name:"), item)
                    }
                  >
                    New File
                  </button>
                  <button
                    className="delete-item"
                    onClick={() => handleDeleteItem(item)}
                  >
                    Delete
                  </button>
                  <button
                    className="rename-item"
                    onClick={() =>
                      handleRenameItem(
                        item,
                        prompt("Enter new folder name:", item.name)
                      )
                    }
                  >
                    Rename
                  </button>
                </span>
                {item.open && renderTreeData(item.children, depth + 1)}
              </li>
            );
          }
        })}
      </ul>
    );
  }

  return <div className="file-explorer">{renderTreeData(treeData)}</div>;
}

export default App;
