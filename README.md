SECTION C :

1.Scheduling periodic tasks with reliability

      If you use small-scale project, cron is good choice. it is a time-based job scheduler in Unix OS, that allows you to schedule scripts or program to run at specific intervals. if you need more advanced and scalable schedule system for large project then use, Apache Airflow, Celery, or Kubernetes CronJobs these  all are tools used for scheduling and running batch jobs or workflows.

2.Encrypting user Bank statements

        Use strong encryption keys - Use strong, randomly generated encryption keys to encrypt the data. The keys should be unique for each user, so that even if one user's data is compromised, the rest of the data is still secure. Encrypt the user's bank statements using the encryption key generated for that user. This can be done using a strong encryption algorithm such as AES-256.


Video Link :

https://drive.google.com/drive/folders/1y2TZRXlsh0uesZyS9vJS75d6eOdCXBPZ?usp=share_link

Deployment Links 

Section A 
 https://sathishkumartsectiona.netlify.app/

Section B - Task 1  
 https://sathishkumartsectionbtask1.netlify.app/

Section B - Task 2  
 https://sathishkumartsectionbtask2.netlify.app/
